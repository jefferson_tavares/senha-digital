var bodyparser = require('body-parser');
var session = require('client-sessions');
var bcrypt = require('bcrypt');
var mysql = require('mysql');
var flash = require('connect-flash');
var moment = require('moment');

var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

moment.locale('pt-br');

app.set('view engine', 'ejs');

app.use(bodyparser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/public'));

app.use(flash());

app.use(session({
	cookieName: 'session',
	secret: 'yT7SIV2QHXHh2r786mkvVdPisggPZJTW8nOI5Z6JZ8oKljRdoHEJdakzgMwp',
	duration: 24 * 60 * 60 * 1000,
	activeDuration: 5 * 60 * 1000
}));

function mysqlConnection() {
	return mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: '',
		database: 'senha_digital',
		port: '3306'
	});
}

function atendenteAuthMiddleware(request, response, next) {
	if(request.url.indexOf('/atendente') != -1) {
		if(request.url == '/atendente/login') {
			next();
		}else {
			if(request.session && request.session.atendente) {
				console.log('Auth middleware ' + request.session.atendente.nome);
				next();
			}else {
				response.redirect('/atendente/login');
			}	
		}
	}else {
		next();
	}
}

function alunoAuthMiddleware(request, response, next) {
	if(request.url.indexOf('/aluno') != -1) {
		if(request.url == '/aluno/login') {
			next();
		}else {
			if(request.session && request.session.aluno) {
				console.log('Auth middleware ' + request.session.aluno.nome);
				next();
			}else {
				response.redirect('/aluno/login');
			}	
		}
	}else {
		next();
	}
}

app.use(atendenteAuthMiddleware);
app.use(alunoAuthMiddleware);

io.on('connect', function(socket) {
	var connection = mysqlConnection();

	var sql = 'SELECT id FROM senha_atendimento WHERE status = 0 limit 5';

	connection.connect(function(error) {
		if(error) throw error

			connection.query(sql, [], function(error, result) {
				if(error) throw error

					io.emit('proxima-senha', {proximas: result});
			});
	});
});

app.get('/sistema/creditos', function(request, response) {

	return response.render('creditos');
});

app.get('/atendente/login', function(request, response) {	
	if(!request.session || !request.session.atendente) {
		return response.render('login');
	}

	return response.redirect('/atendente/dashboard-atendimento');
});

app.post('/atendente/login', function(request, response) {
	var connection = mysqlConnection();

	connection.connect(function(error){
		if(error) throw error

			var sql = 'SELECT id, nome, senha, mesa, fk_id_setor as setor FROM operador WHERE email = ? limit 1';

		connection.query(sql, [request.body.email], function(error, result)  {
			if(error) throw error

				if(result.length == 1) {
					if(bcrypt.compareSync(request.body.senha, result[0].senha)) {
						request.session.atendente = result[0];

						console.log('Logged in ' + result[0].nome)

						return response.redirect('/atendente/dashboard-atendimento');
					}
				}

				return response.redirect('/atendente/login');
			});
	});
});

app.get('/atendente/logout', function(request, response) {
	request.session.reset();
	response.redirect('/atendente/login');
});

app.get('/atendente/dashboard-atendimento', function(request, response) {
	var connection = mysqlConnection();

	var sql = 'select s.id as token, s.solicitacao from senha_atendimento s ' +
	'inner join aluno a on a.id = s.fk_id_aluno ' +
	' where s.status = 0 and fk_id_setor = ?';

	connection.query(sql, [request.session.atendente.setor], function(error, result) {
		if(error) throw error

			response.render('dashboard-atendimento', {tokens: result, moment: moment});
	});
});

app.get('/atendente/realizar-atendimento', function(request, response) {
	var connection = mysqlConnection();
	
	connection.connect(function(error) {
		if(error) throw error

			var sql = 'SELECT s.id, s.solicitacao, s.inicio_atendimento, a.nome as aluno FROM senha_atendimento s ' +
		'INNER JOIN aluno a ON a.id = s.fk_id_aluno ' +
		'WHERE s.fk_id_operador = ? AND s.status = 2';

		connection.query(sql, [request.session.atendente.id], function(error, result) {
			if(error) throw error

				console.log('Result: ' + result.length);
			console.log(result[0]);

			if(result.length > 0) {
				response.render('realizar-atendimento', {token: result[0], moment: moment});
			}else {
				var sql = 'SELECT * FROM senha_atendimento WHERE status = 0 AND fk_id_setor = ? LIMIT 1';

				connection.query(sql, [request.session.atendente.setor], function(error, result) {
					if(error) throw error

						if(result.length == 1){
							var sql = 'UPDATE senha_atendimento SET status = 2, ' + 
							'inicio_atendimento = now(), '+
							'fk_id_operador = ? ' +
							'WHERE id = ?';

							var idProximoAtendimento = result[0].id;

							connection.query(sql, [request.session.atendente.id, idProximoAtendimento], function(error, result){
								if(error) throw error

									var sql = 'SELECT s.id, s.solicitacao, s.inicio_atendimento, a.nome as aluno FROM senha_atendimento s ' +
								'INNER JOIN aluno a ON a.id = s.fk_id_aluno ' + 
								'WHERE s.id = ?';

								connection.query(sql, [idProximoAtendimento], function(error, result) {
									if(error) throw error

									var sql = 'SELECT id FROM senha_atendimento WHERE status = 0 limit 5';

									connection.query(sql, [], function(error, result) {
										io.emit('proxima-senha', {proximas: result});
									});

									response.render('realizar-atendimento', {token: result[0], moment: moment});
								});
							});
						}else {
							request.flash('status', 'Não existem senhas a serem atendidas.');
							response.redirect('/atendente/dashboard-atendimento');
						}
					});
			}
		});
	});
});

app.get('/atendente/concluir-atendimento/:token', function(request, response) {
	var connection = mysqlConnection();
	
	connection.connect(function(error){
		if(error) throw error

			var sql = 'UPDATE senha_atendimento SET status = 1, conclusao_atendimento = now() WHERE id = ?';

		console.log('Token: ' + request.params.token);
		
		connection.query(sql, [request.params.token], function(error, result) {
			if(error) throw error
				

				if(request.query.iniciar_atendimento) {
					response.redirect('/atendente/realizar-atendimento');
				}else {
					response.redirect('/atendente/dashboard-atendimento');
				}
			});
	});
});

app.get('/atendente/autenticar-token/:token', function(request, response){
	return response.render('autenticar-token', {token: request.params.token});
});

app.post('/atendente/autenticar-token', function(request, response) {
	var connection = mysqlConnection();
	
	connection.connect(function(error) {
		if(error) throw error

			var sql = 'SELECT senha FROM aluno WHERE id = (SELECT fk_id_aluno FROM senha_atendimento WHERE id = ?)';

		connection.query(sql, [request.body.token], function(error, result){
			if(error) throw error

				if(bcrypt.compareSync(request.body.senha, result[0].senha)) {
					response.redirect('/atendente/realizar-atendimento');
				}else {
					response.redirect('/atendente/autenticar-token/' + request.body.token);
				}
			});
	});
});





//TODO adicionar admin middleware
app.get('/admin/cadastrar-atendente', function(request, response){
	response.render('cadastrar-atendente');
});

app.post('/admin/cadastrar-atendente', function(request, response){
	var connection = mysqlConnection();

	//TODO renomear tabela
	var sql = 'INSERT INTO operador(nome, senha, mesa, fk_id_setor) VALUES(?,?,?,?)';

	var body = request.body;

	connection.query(sql, [body.nome, bcrypt.hashSync(body.senha, 10), body.mesa, body.setor], function(error, result){
		if(error) throw error

			response.send("Salvo com sucesso: " + result.insertId);
	});
});

app.get('/admin/cadastrar-setor', function(request, response){
	response.render('cadastrar-setor');
});

app.post('/admin/cadastrar-setor', function(request, response){
	var connection = mysqlConnection();

	var sql = 'INSERT INTO setor(nome) VALUES (?)';

	connection.query(sql, [request.body.nome], function(error, result) {
		if(error) throw error

			response.redirect('/admin/dashboard');
	});
});


app.get('/admin/cadastrar-aluno', function(request, response) {
	response.render('cadastrar-aluno');
});

app.post('/admin/cadastrar-aluno', function(request, response) {
	var connection = mysqlConnection();

	var sql = 'INSERT INTO aluno(nome, matricula, senha) VALUES(?,?,?)';

	connection.query(sql, [request.body.nome, request.body.matricula, bcrypt.hashSync(request.body.senha, 10)], 
		function(error, result) {
			if(error) throw error

				response.redirect('/admin/dashboard');
		}); 
});








// criaçao de rota tenho que passa  a pagina /aluno/login nome a parte 
// resquet requisiçao
//response devolve a resposta 
app.get('/aluno/login',function(request,response){
	//pega a pagina e motra para o usuario 
	response.render('login-aluno');

});

// rota da pagina de login
app.post('/aluno/login',function(request,response){

	var conexao = mysqlConnection();

	var sql = "select * from aluno where matricula = ?";

	console.log('Recebendo autenticacao aluno');
	
	conexao.connect(function(error){
      	// erro de conexao
      	if(error) throw error
            //parte complica onde se faz de tras pra frente do java 
        conexao.query(sql,[request.body.matricula],function(error,result){
        	if(error) throw error

        		if(result.length == 0 ){
        			console.log('Nenhuma matricula encontrada')
               	// serve para o redirecionamento de logica 
               	response.redirect('/aluno/login');

               }else{
               	//sessao local

               	console.log('Senha enviada: ' + request.body.senha);
               	console.log('Senha salva: ' + result[0].senha);

               	if(bcrypt.compareSync(request.body.senha, result[0].senha)){
               		console.log('Aluno logado');
               		request.session.aluno = result[0];
               		//precisa da rota pra funciona 
               		//o erro que aprace e o "Cannot GET /aluno/acompanhamento-senha" siginfica que nao contem a roto
               		response.redirect('/aluno/acompanhamento-senha');
               	} else{
               		console.log('Senha inválida')
               		response.redirect('/aluno/login');

               	}
               }      
           });

    });
});

//rota paara a pagina de acapanhameto de senha 
app.get('/aluno/acompanhamento-senha/:aluno?*',function(request,response){
	if(request.params.aluno) {
		var conexao = mysqlConnection();
		// altera id do aluno
		var sql = "select senha_atendimento.id,setor.nome from senha_atendimento inner join setor on setor.id  = senha_atendimento.fk_id_setor  where senha_atendimento.status = 0 and senha_atendimento.fk_id_aluno = ? ";

		conexao.connect(function(error){

			if(error) throw error
					//parte de sessao 
				conexao.query(sql,[request.params.aluno],function(error,result){
					if(error) throw error

					//pega a pagina e motra para o usuario 
				response.send(result);
			});
			});
	}else {
		response.render('acompanhamento-senha', {id: request.session.aluno.id});
	}
});



//rota de geraçao de senha 
app.get('/aluno/gerar-senha/:setor',function(request,response){
	var conexao =  mysqlConnection();
	var sql = "insert into senha_atendimento(solicitacao,status,fk_id_aluno,fk_id_setor)values(now(),0,?,?)";

	conexao.connect(function(error){
		if(error) throw error

			conexao.query(sql,[request.session.aluno.id,request.params.setor],function(error,result){
				if(error) throw error

					response.redirect('/aluno/acompanhamento-senha/');
			});
	});

});

//rota de seleçao de setor 
app.get('/aluno/selecionar-setor',function(request,response){
	var conexao = mysqlConnection();
	var sql = "select * from setor";
	conexao.connect(function(error){
		if(error) throw error 

			conexao.query(sql,[],function(error,result){
        	//render mostra a pagina de seleçao de setor  reder serve pra isso 
        	response.render('selecionar-setor',{setores: result});
        });
	});
});

app.get('/aluno/logout',function(request,response){
	request.session.reset();
	response.redirect('/aluno/login');

})














server.listen(3000, function() {
	console.log('listening');
});